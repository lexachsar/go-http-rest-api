package teststore

import (
	"gitlab.com/lexachsar/http-rest-api/internal/app/model"
	"gitlab.com/lexachsar/http-rest-api/internal/app/store"
)

// A struct representing the test data storage.
type Store struct {
	// A UserRepository to work with a User model
	userRepository *UserRepository
}

// Create a new Store entity from config
func New() *Store {
	return &Store{}
}

// A function returns a pointer to UserRepository struct
// that enables a storage to work with a User model.
func (s *Store) User() store.UserRepository {
	// If userRepository already exists, just return it
	if s.userRepository != nil {
		return s.userRepository
	}

	// If userRepository don't exists, create a
	// new repository instance.
	s.userRepository = &UserRepository{
		store: s,
		users: make(map[string]*model.User),
	}

	// Return a new repository instance.
	return s.userRepository
}
