package teststore_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/lexachsar/http-rest-api/internal/app/model"
	"gitlab.com/lexachsar/http-rest-api/internal/app/store"
	"gitlab.com/lexachsar/http-rest-api/internal/app/store/teststore"
	"testing"
)

// Test Create method of UserRepository struct.
// Check if User is actually created on Create method run and no error occurs.
func TestUserRepository_Create(t *testing.T) {
	s := teststore.New()

	u := model.TestUser(t)

	// Check if any error occurs during user creation.
	assert.NoError(t, s.User().Create(u))
	// Check if user is actually created.
	assert.NotNil(t, u)
}

// Test FindByEmail method of UserRepository struct.
// This method tests if FindByEmail returns an error when attempt
// to find not existing user is done.
// Also this method checks if FindByEmail returns an actual user when
// attempt to find an existing user is done.
func TestUserRepository_FindByEmail(t *testing.T) {
	s := teststore.New()

	// Create a var with a test email
	email := "user@example.org"

	// Find non existing user by email
	_, err := s.User().FindByEmail(email)
	// Check if returned error is "ErrRecordNotFound"
	assert.EqualError(t, err, store.ErrRecordNotFound.Error())

	u := model.TestUser(t)
	u.Email = email

	// Create a User
	err = s.User().Create(u)

	// Try to find a user by email.
	u, err = s.User().FindByEmail(email)
	// Check if error didn't occur.
	assert.NoError(t, err)
	// Check if founded used is not nul.
	assert.NotNil(t, u)
}
