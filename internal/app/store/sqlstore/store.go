package sqlstore

import (
	"database/sql"
	"gitlab.com/lexachsar/http-rest-api/internal/app/store"

	_ "github.com/lib/pq" // PG driver
)

// A struct representing the data storage.
type Store struct {
	// A sql.DB struct to connect to the database
	db *sql.DB
	// A UserRepository to work with a User model
	userRepository *UserRepository
}

// Create a new Store entity from config
func New(db *sql.DB) *Store {
	return &Store{
		db: db,
	}
}

// A function returns a pointer to UserRepository struct
// that enables a storage to work with a User model.
func (s *Store) User() store.UserRepository {
	// If userRepository already exists, just return it
	if s.userRepository != nil {
		return s.userRepository
	}

	// If userRepository don't exists, create a
	// new repository instance.
	s.userRepository = &UserRepository{
		store: s,
	}

	// Return a new repository instance.
	return s.userRepository
}
