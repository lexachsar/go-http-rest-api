package sqlstore_test

import (
	"os"
	"testing"
)

var (
	// Database URL global variable
	databaseURL string
)

// A main function to
func TestMain(m *testing.M) {
	// Get database url from environmental variable
	databaseURL = os.Getenv("DATABASE_URL")

	// If environmental variable is not set, set the default value
	if databaseURL == "" {
		databaseURL = "host=localhost dbname=restapi_test sslmode=disable"
	}

	// Exit main function with m.Run() method call.
	os.Exit(m.Run())
}
