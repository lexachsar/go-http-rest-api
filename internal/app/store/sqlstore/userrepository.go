package sqlstore

import (
	"database/sql"
	"gitlab.com/lexachsar/http-rest-api/internal/app/model"
	"gitlab.com/lexachsar/http-rest-api/internal/app/store"
)

// A struct that enables Store struct to work with a User model.
type UserRepository struct {
	store *Store
}

// Create a User entity in the database
func (r *UserRepository) Create(u *model.User) error {

	// Try to validate a user fields
	if err := u.Validate(); err != nil {
		return err
	}

	// Run BeforeCreate method of User model which
	// encrypts a user password.
	if err := u.BeforeCreate(); err != nil {
		return err
	}

	// Create a User entity and return a user ID
	// Then using Scan method map returned ID to a User struct's ID field.
	return r.store.db.QueryRow(
		"INSERT INTO users (email, encrypted_password) VALUES ($1, $2) RETURNING id",
		//
		u.Email,
		u.EncryptedPassword,
	).Scan(&u.ID)
}

// FindByEmail find a User by email
func (r *UserRepository) FindByEmail(email string) (*model.User, error) {
	// Create an empty User entity
	u := &model.User{}

	// Select a user from the database by email
	// and map it's fields to a User entity.
	if err := r.store.db.QueryRow(
		"SELECT id, email, encrypted_password FROM users WHERE email = $1",
		email,
	).Scan(
		&u.ID,
		&u.Email,
		&u.EncryptedPassword,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return u, nil
}
