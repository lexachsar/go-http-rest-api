package apiserver

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/sessions"
	"github.com/stretchr/testify/assert"
	"gitlab.com/lexachsar/http-rest-api/internal/app/model"
	"gitlab.com/lexachsar/http-rest-api/internal/app/store/teststore"
	"net/http"
	"net/http/httptest"
	"testing"
)

// A test for HandleUsersCreate handler function
func TestServer_HandleUsersCreate(t *testing.T) {
	// Create a new server instance from a in-memory teststore
	s := newServer(teststore.New(), sessions.NewCookieStore([]byte("secret")))

	testCases := []struct {
		name         string
		payload      interface{}
		expectedCode int
	}{
		{
			name: "valid",
			payload: map[string]string{
				"email":    "user@example.org",
				"password": "password",
			},
			expectedCode: http.StatusCreated,
		},
		{
			name:         "invalid payload",
			payload:      "invalid",
			expectedCode: http.StatusBadRequest,
		},
		{
			name: "invalid params",
			payload: map[string]string{
				"email": "invalid",
			},
			expectedCode: http.StatusUnprocessableEntity,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Initialise httptest recorder
			rec := httptest.NewRecorder()

			b := &bytes.Buffer{}

			json.NewEncoder(b).Encode(tc.payload)

			// initialise a new http request
			req, _ := http.NewRequest(http.MethodPost, "/users", b)

			// Run serve HTTP method
			s.ServeHTTP(rec, req)

			// Check if return code recorded by httptest
			// recorder is expected by the test case.
			assert.Equal(t, tc.expectedCode, rec.Code)
		})
	}
}

func TestServer_HandleSessionsCreate(t *testing.T) {
	u := model.TestUser(t)
	store := teststore.New()
	store.User().Create(u)

	// Create a new server instance from a in-memory teststore
	s := newServer(store, sessions.NewCookieStore([]byte("secret")))

	testCases := []struct {
		name         string
		payload      interface{}
		expectedCode int
	}{
		{
			name: "valid",
			payload: map[string]string{
				"email":    u.Email,
				"password": u.Password,
			},
			expectedCode: http.StatusOK,
		},
		{
			name:         "invalid payload",
			payload:      "invalid",
			expectedCode: http.StatusBadRequest,
		},
		{
			name: "invalid email",
			payload: map[string]string{
				"email":    "invalid",
				"password": u.Password,
			},
			expectedCode: http.StatusUnauthorized,
		},
		{
			name: "invalid password",
			payload: map[string]string{
				"email":    u.Email,
				"password": "invalid",
			},
			expectedCode: http.StatusUnauthorized,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Initialise httptest recorder
			rec := httptest.NewRecorder()

			b := &bytes.Buffer{}

			json.NewEncoder(b).Encode(tc.payload)

			// initialise a new http request
			req, _ := http.NewRequest(http.MethodPost, "/sessions", b)

			// Run serve HTTP method
			s.ServeHTTP(rec, req)

			// Check if return code recorded by httptest
			// recorder is expected by the test case.
			assert.Equal(t, tc.expectedCode, rec.Code)
		})
	}
}
