package apiserver

import (
	"database/sql"
	"github.com/gorilla/sessions"
	"gitlab.com/lexachsar/http-rest-api/internal/app/store/sqlstore"
	"net/http"
)

// Start function to start apiserver
func Start(config *Config) error {
	// Initialize the new database
	db, err := newDB(config.DatabaseURL)

	if err != nil {
		return err
	}

	// Defer the database closing
	defer db.Close()

	// Initialize the new Store instance from
	store := sqlstore.New(db)

	sessionStore := sessions.NewCookieStore([]byte(config.SessionKey))

	// Initialise the new store
	s := newServer(store, sessionStore)

	return http.ListenAndServe(config.BindAddr, s)
}

// Initialise a new database.
func newDB(databaseURL string) (*sql.DB, error) {
	// Create a connection to the database.
	db, err := sql.Open("postgres", databaseURL)

	if err != nil {
		return nil, err
	}

	// Try to ping the database.
	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
