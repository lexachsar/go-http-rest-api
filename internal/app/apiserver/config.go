package apiserver

// Config a config type for the APIServer type.
type Config struct {
	// URL on wich web server is started
	BindAddr string `toml:"bind_addr"`
	// Log level for the logrus package
	LogLevel    string `toml:"log_level"`
	DatabaseURL string `toml:"database_url"`
	SessionKey  string `toml:"session_key"`
}

// NewConfig a method to initialize default config.
func NewConfig() *Config {
	return &Config{
		BindAddr: ":8080",
		LogLevel: "debug",
	}
}
