package model

import "testing"

// TestUser creates a user object for the test purpose.
func TestUser(t *testing.T) *User {
	return &User{
		Email:    "user@example.org",
		Password: "password",
	}
}
