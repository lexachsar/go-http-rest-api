# Create the user

## Invalid user creation request example 

```bash
$ curl -X POST --data '{"email": "invalid"}' http://localhost:8080/users
{"error":"email: must be a valid email address; password: cannot be blank."}
```

## Valid user creation request example

```bash
$ curl -X POST --data '{"email": "user@example.org", "password": "password"}' http://localhost:8080/users
{"id":1,"email":"user@example.org"}
```
