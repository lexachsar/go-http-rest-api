// main script

package main

import (
	"flag"
	"github.com/BurntSushi/toml"
	"log"

	"gitlab.com/lexachsar/http-rest-api/internal/app/apiserver"
)

var (
	configPath string
)

func init() {
	flag.StringVar(
		&configPath,
		"config-path",
		"configs/apiserver.toml",
		"path to config file",
	)
}

func main() {
	// Parse command line flags
	flag.Parse()

	// Initialize new APIServer Config
	config := apiserver.NewConfig()

	// Decode a TOML config file
	_, err := toml.DecodeFile(configPath, config)
	// If error is met during decoding,
	// stop the program with log message.
	if err != nil {
		log.Fatal(err)
	}

	// Try to start the server. If any errors occur,
	// stop execution and log the errors.
	if err := apiserver.Start(config); err != nil {
		log.Fatal(err)
	}
}
